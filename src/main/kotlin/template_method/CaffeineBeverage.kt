package template_method

abstract class CaffeineBeverage {

    fun prepareRecipe() {
        boilWater()
        brew()
        pourInCup()
        addCondiments()
    }

    abstract fun brew()

    abstract fun addCondiments()

    open fun boilWater() {
        println("Boiling water")
    }

    open fun pourInCup() {
        println("Pouring into cup")
    }
}
