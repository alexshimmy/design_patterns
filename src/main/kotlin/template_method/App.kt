package template_method

fun main() {
    val coffee: CaffeineBeverage = Coffee()
    coffee.prepareRecipe()
}