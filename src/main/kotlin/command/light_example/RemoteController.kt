package command.light_example

class RemoteController {

    private var command: Command = NoCommand()
    private var undoCommand: Command = NoCommand()

    fun setCommand(command: Command) {
        this.command = command
    }

    fun pressButton() {
        command.execute()
        undoCommand = command
    }

    fun undo() {
        undoCommand.undo()
    }
}