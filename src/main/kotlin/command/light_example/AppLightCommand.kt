package command.light_example

fun main() {
    val remoteController = RemoteController()
    val light = Light()

    remoteController.setCommand(TurnLightOnCommand(light))
    remoteController.pressButton()

    remoteController.setCommand(TurnLightOffCommand(light))
    remoteController.pressButton()

    remoteController.undo()
}