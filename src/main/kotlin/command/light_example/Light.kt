package command.light_example

class Light {

    fun switchOn() {
        println("Light is on")
    }

    fun switchOff() {
        println("Light is off")
    }
}