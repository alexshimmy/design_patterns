package command.light_example

interface Command {

    fun execute()

    fun undo()
}