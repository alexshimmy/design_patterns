package command.light_example

class TurnLightOffCommand(private val light: Light) : Command {

    override fun execute() {
        light.switchOff()
    }

    override fun undo() {
        light.switchOn()
    }
}