package command.light_example

class NoCommand : Command {
    override fun execute() {}

    override fun undo() {}
}