package command.light_example

class TurnLightOnCommand(private val light: Light) : Command {

    override fun execute() {
        light.switchOn()
    }

    override fun undo() {
        light.switchOff()
    }
}