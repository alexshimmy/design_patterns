package command.computer_example

//Receiver
class Computer {
    fun start() {
        println("Computer has been started")
    }

    fun stop() {
        println("Computer has been stopped")
    }

    fun reset() {
        println("Computer has been reset")
    }
}