package command.computer_example

class ResetCommand(private val computer: Computer) : Command {

    override fun execute() {
        computer.reset()
    }
}