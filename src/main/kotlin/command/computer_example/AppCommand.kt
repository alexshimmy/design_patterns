package command.computer_example

fun main() {

    val computer = Computer()
    val user = User(start = StartCommand(computer), stop = StopCommand(computer), reset = ResetCommand(computer))

    user.startComputer()
    user.stopComputer()
    user.resetComputer()
}