package command.computer_example

class StartCommand(private val computer: Computer) : Command {

    override fun execute() {
        computer.start()
    }
}