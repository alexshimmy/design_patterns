package command.computer_example

class StopCommand(private val computer: Computer) : Command {

    override fun execute() {
        computer.stop()
    }
}