package command.computer_example

interface Command {

    fun execute()
}