package command.computer_example

//Invoker
class User(private val start: Command, private val stop: Command, private val reset: Command) {

    fun startComputer() {
        start.execute()
    }

    fun stopComputer() {
        stop.execute()
    }

    fun resetComputer() {
        reset.execute()
    }
}