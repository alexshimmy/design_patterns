package decorator

class Mocha(private val beverage: Beverage) : CondimentDecorator() {
    override fun cost(): Double {
        return .20 + beverage.cost()
    }

    init {
        description = beverage.description + ", Mocha"
    }
}