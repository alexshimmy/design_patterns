package decorator

abstract class Beverage {
    var description = "Unknown description"

    abstract fun cost(): Double
}