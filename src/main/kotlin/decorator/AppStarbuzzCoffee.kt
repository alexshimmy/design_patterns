package decorator

fun main() {
    var beverage: Beverage = Espresso()
    println("Beverage description: ${beverage.description}")
    println("Beverage cost: ${beverage.cost()}")

    beverage = Mocha(beverage)
    println("Beverage description: ${beverage.description}")
    println("Beverage cost: ${beverage.cost()}")

    beverage = Milk(beverage)
    println("Beverage description: ${beverage.description}")
    println("Beverage cost: ${beverage.cost()}")
}