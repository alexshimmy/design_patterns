package decorator

class Milk(private val beverage: Beverage): CondimentDecorator() {
    override fun cost(): Double {
        return beverage.cost() + .25
    }

    init {
        description = beverage.description + ", Milk"
    }
}