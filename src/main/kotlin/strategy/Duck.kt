package strategy

abstract class Duck(private val flyBehavior: FlyBehavior, private val quackBehavior: QuackBehavior) {

    fun swim() {
        println("Swim")
    }

    fun performFly() {
        flyBehavior.fly()
    }

    fun performQuack() {
        quackBehavior.quack()
    }
}