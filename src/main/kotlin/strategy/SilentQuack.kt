package strategy

class SilentQuack: QuackBehavior {
    override fun quack() {
        println("<silent>")
    }
}