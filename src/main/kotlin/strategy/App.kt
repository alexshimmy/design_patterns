package strategy

fun main() {
    val duck: Duck = ModelDuck()
    duck.swim()
    duck.performFly()
    duck.performQuack()
}