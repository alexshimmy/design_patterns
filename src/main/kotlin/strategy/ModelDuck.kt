package strategy

class ModelDuck : Duck(FlyNoWay(), SilentQuack())