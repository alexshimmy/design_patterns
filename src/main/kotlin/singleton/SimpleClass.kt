package singleton

class SimpleClass private constructor() {

    companion object {
        @Volatile private var instance: SimpleClass? = null

        fun getInstance(): SimpleClass {
            val checkInstance = instance
            if (checkInstance != null) {
                return checkInstance
            }

            return synchronized(this) {
                val checkInstanceAgain = instance
                if (checkInstanceAgain != null) {
                    checkInstanceAgain
                } else {
                    instance = SimpleClass()
                    instance!!
                }
            }
        }
    }
}