package singleton

fun main() {
    SingletonKotlinObject.singletonFun() //is initialized after accessing the object

    val firstSimpleClassInstance = SimpleClass.getInstance()
    println("First singleton instance: $firstSimpleClassInstance")

    val secondSimpleClassInstance = SimpleClass.getInstance()
    println("Second singleton instance: $secondSimpleClassInstance")

}