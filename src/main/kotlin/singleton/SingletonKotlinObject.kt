package singleton

object SingletonKotlinObject {
    init {
        println("${this.javaClass} is initialized")
    }

    fun singletonFun() {
        println("Singleton function")
    }
}