package observer

class CurrentConditionsDisplay(weatherData: WeatherData) : Observer, DisplayElement {

    var temp = 0.0f
    var humidity = 0.0f

    init {
        weatherData.registerObserver(this)
    }

    override fun display() {
        println("Current conditions: $temp F degrees and $humidity% humidity")
    }

    override fun update(temp: Float, humidity: Float, pressure: Float) {
        this.temp = temp
        this.humidity = humidity
        display()
    }
}