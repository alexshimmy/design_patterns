package observer

fun main() {
    val weatherData = WeatherData()
    val currentConditionsDisplay = CurrentConditionsDisplay(weatherData)
    weatherData.setMeasurements(temp = 10.2f, humidity = 64.5f, pressure = 10f)
    weatherData.removeObserver(currentConditionsDisplay)
    weatherData.setMeasurements(temp = 10.4f, humidity = 68.1f, pressure = 11f)
    weatherData.registerObserver(currentConditionsDisplay)
    weatherData.setMeasurements(temp = 12.2f, humidity = 70.5f, pressure = 14f)
}
