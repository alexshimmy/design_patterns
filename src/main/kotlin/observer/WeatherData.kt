package observer

class WeatherData : Subject {

    var observers: ArrayList<Observer> = arrayListOf()
    var temp: Float = 0.0f
    var humidity = 0.0f
    var pressure = 0.0f

    override fun registerObserver(observer: Observer) {
        observers.add(observer)
    }

    override fun removeObserver(observer: Observer) {
        observers.remove(observer)
    }

    override fun notifyObservers() {
        observers.forEach { it.update(temp, humidity, pressure) }
    }

    private fun measurementsChanged() {
        notifyObservers()
    }

    fun setMeasurements(temp: Float, humidity: Float, pressure: Float) {
        this.temp = temp
        this.humidity = humidity
        this.pressure = pressure
        measurementsChanged()
    }
}