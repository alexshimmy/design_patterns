package composite

class Menu(val name: String, val description: String) : MenuComponent() {

    private val menuComponents = arrayListOf<MenuComponent>()

    override fun add(menuComponent: MenuComponent) {
        menuComponents.add(menuComponent)
    }

    override fun remove(menuComponent: MenuComponent) {
        menuComponents.remove(menuComponent)
    }

    override fun getChild(i: Int): MenuComponent {
        return menuComponents[i]
    }

    override fun print() {
        println(name)
        println(description)
        println("--------------")

        val iterator = menuComponents.iterator()
        while (iterator.hasNext()) {
            iterator.next().print()
        }
    }
}