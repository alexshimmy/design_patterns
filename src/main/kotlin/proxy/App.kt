package proxy

fun main() {
    val displayTimetable = DisplayTimetable()
    displayTimetable.printTimetable()
}