package proxy

class DisplayTimetable {

    private val timetableTrains: TimetableTrains = TimetableElectricTrainsProxy()

    fun printTimetable() {
        val timetable = timetableTrains.getTimetable()
        var tmpArr: List<String>

        println("Поезд\t\tОткуда\t\tКуда\t\tВремя отправления\tВремя прибытия\t\tВремя в пути")
        for (i in timetable.indices) {
            tmpArr = timetable[i].split(";".toRegex())
            println("" +
                    "${tmpArr[0]}\t\t" +
                    "${tmpArr[1]}\t\t" +
                    "${tmpArr[2]}\t\t" +
                    "${tmpArr[3]}\t\t\t\t" +
                    "${tmpArr[4]}\t\t\t\t" +
                    "${tmpArr[5]}\n"
            )
        }
    }
}