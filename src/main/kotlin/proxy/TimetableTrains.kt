package proxy

interface TimetableTrains {

    fun getTimetable(): ArrayList<String>
    fun getTrainDepartureTime(trainId: String): String
}
