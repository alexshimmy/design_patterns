package proxy

class TimetableElectricTrains : TimetableTrains {

    override fun getTimetable(): ArrayList<String> {
        val list = arrayListOf<String>()

        list.add("9B-6854;Лондон;Прага;13:43;21:15;07:32")
        list.add("BA-1404;Париж;Грац;14:25;21:25;07:00")
        list.add("9B-8710;Прага;Вена;04:48;08:49;04:01")
        list.add("9B-8122;Прага;Грац;04:48;08:49;04:01")

        return list
    }

    override fun getTrainDepartureTime(trainId: String): String {
        val timetable: ArrayList<String> = getTimetable()

        return timetable.find { it.startsWith(trainId) } ?: ""
    }
}