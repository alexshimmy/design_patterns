package proxy

class TimetableElectricTrainsProxy : TimetableTrains {

    private var timetableTrains: TimetableTrains = TimetableElectricTrains()
    private var timetableCache: ArrayList<String>? = null

    override fun getTimetable(): ArrayList<String> {
        if (timetableCache == null) {
            timetableCache = timetableTrains.getTimetable()
        }

        return timetableCache!!
    }

    override fun getTrainDepartureTime(trainId: String): String {
        if (timetableCache == null) {
            timetableCache = timetableTrains.getTimetable()
        }

        return timetableCache!!.find { it.startsWith(trainId) } ?: ""
    }

    fun clearCache() {
        timetableCache = null
    }
}