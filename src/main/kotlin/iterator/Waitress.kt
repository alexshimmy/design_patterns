package iterator

class Waitress(private val pancakeHouseMenu: PancakeHouseMenu) {

    fun printMenu() {
        val pancakeIterator = pancakeHouseMenu.createIterator()
        println("############# Pancake House menu:")
        printMenu(pancakeIterator)
    }

    private fun printMenu(iterator: Iterator<MenuItem>) {
        while (iterator.hasNext()) {
            println(iterator.next())
        }
    }
}