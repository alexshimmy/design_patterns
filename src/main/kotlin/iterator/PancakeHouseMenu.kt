package iterator

class PancakeHouseMenu {
    private val menuItems = arrayListOf<MenuItem>()

    init {
        addItem(
            "K&B Pancake Breakfast",
            "Pancakes with scrambled eggs, and toast",
            true,
            2.99
        )

        addItem(
            "Blueberry Pancakes",
            "Pancakes made with fresh blueberries",
            false,
            3.59
        )
    }

    fun createIterator(): PancakeHouseMenuIterator {
        return PancakeHouseMenuIterator(menuItems)
    }

    private fun addItem(name: String, description: String, vegetarian: Boolean, price: Double) {
        menuItems.add(MenuItem(name, description, vegetarian, price))
    }
}