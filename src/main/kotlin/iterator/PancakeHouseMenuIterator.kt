package iterator

class PancakeHouseMenuIterator(private val menuItems: ArrayList<MenuItem>) : Iterator<MenuItem> {
    private var position: Int = 0

    override fun hasNext(): Boolean {
        return position < menuItems.size
    }

    override fun next(): MenuItem {
        val menuItem = menuItems[position]
        position += 1

        return menuItem
    }
}