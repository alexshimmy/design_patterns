package iterator

fun main() {
    val pancakeHouseMenu = PancakeHouseMenu()
    val waitress = Waitress(pancakeHouseMenu)

    waitress.printMenu()
}