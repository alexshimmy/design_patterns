package adapter

fun main() {
    val card = MemoryCard()
    val cardReader: USB = CardReader(card)

    cardReader.connectWithUsbCable()
}