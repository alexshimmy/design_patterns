package adapter

class CardReader(private val memoryCard: MemoryCard) : USB {

    override fun connectWithUsbCable() {
        memoryCard.insert()
        memoryCard.copyData()
    }
}
