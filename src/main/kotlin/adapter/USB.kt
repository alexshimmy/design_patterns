package adapter

interface USB {

    fun connectWithUsbCable()
}
