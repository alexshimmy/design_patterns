package adapter

class MemoryCard {

    fun insert() {
        println("Memory card has been inserted")
    }

    fun copyData() {
        println("Data has been copied")
    }
}