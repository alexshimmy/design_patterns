package factory

import factory.modern.ModernFurnitureFactory
import factory.victorian.VictorianFurnitureFactory

fun main() {
    val modernFurniture = ModernFurnitureFactory()
    modernFurniture.createChair()
    modernFurniture.createSofa()
    modernFurniture.createCoffeeTable()

    val victorianFurniture = VictorianFurnitureFactory()
    victorianFurniture.createChair()
    victorianFurniture.createSofa()
    victorianFurniture.createCoffeeTable()
}