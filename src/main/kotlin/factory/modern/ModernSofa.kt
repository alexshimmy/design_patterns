package factory.modern

import factory.Sofa

class ModernSofa : Sofa {
    override val name = "Modern Sofa"
}
