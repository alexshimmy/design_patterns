package factory.modern

import factory.Chair

class ModernChair : Chair {
    override val name = "Modern chair"
}