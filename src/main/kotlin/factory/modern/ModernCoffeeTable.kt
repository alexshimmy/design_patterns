package factory.modern

import factory.CoffeeTable

class ModernCoffeeTable : CoffeeTable {
    override val name = "Modern Coffee Table"
}