package factory.modern

import factory.Chair
import factory.CoffeeTable
import factory.FurnitureFactory
import factory.Sofa

class ModernFurnitureFactory : FurnitureFactory {
    override fun createChair(): Chair {
        return ModernChair().also { println("${it.name} has been created") }
    }

    override fun createCoffeeTable(): CoffeeTable {
        return ModernCoffeeTable().also { println("${it.name} has been created") }
    }

    override fun createSofa(): Sofa {
        return ModernSofa().also { println("${it.name} has been created") }
    }
}