package factory.victorian

import factory.Sofa

class VictorianSofa : Sofa {
    override val name = "Victorian Sofa"
}
