package factory.victorian

import factory.CoffeeTable

class VictorianCoffeeTable : CoffeeTable {
    override val name = "Victorian Coffee Table"
}