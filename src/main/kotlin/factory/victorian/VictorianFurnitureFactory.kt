package factory.victorian

import factory.Chair
import factory.CoffeeTable
import factory.FurnitureFactory
import factory.Sofa

class VictorianFurnitureFactory : FurnitureFactory {
    override fun createChair(): Chair {
        return VictorianChair().also { println("${it.name} has been created") }
    }

    override fun createCoffeeTable(): CoffeeTable {
        return VictorianCoffeeTable().also { println("${it.name} has been created") }
    }

    override fun createSofa(): Sofa {
        return VictorianSofa().also { println("${it.name} has been created") }
    }
}