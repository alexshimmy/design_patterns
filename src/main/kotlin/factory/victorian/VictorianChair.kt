package factory.victorian

import factory.Chair

class VictorianChair : Chair {
    override val name = "Victorian chair"
}