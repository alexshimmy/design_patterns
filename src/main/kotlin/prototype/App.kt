package prototype

data class Bike(
    var bikeType: String = "Standard",
    var gears: Int = 0,
    var model: String? = null
) {

    fun clone(): Bike {
        return Bike(this.bikeType, this.gears, this.model)
    }

    fun makeAdvanced() {
        bikeType = "Advanced"
        model = "Jaguar"
        gears = 6
    }
}

fun makeJaguar(basicBike: Bike): Bike {
    basicBike.makeAdvanced()
    return basicBike
}

fun main() {
    val bike = Bike(bikeType = "SUPER BIKE", model = "Model 3000")
    println("Default bike: $bike")
    val basicBike = bike.clone()
    println("Cloned bike: $basicBike")
    val advancedBike = makeJaguar(basicBike)
    println("Advanced bike (Jaguar): $advancedBike")
}