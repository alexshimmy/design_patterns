package state

data class GumballMachine(var numberGumballs: Int) {

    val soldOutState: State = SoldOutState(this)
    val noQuarterState: State = NoQuarterState(this)
    val hasQuarterState: State = HasQuarterState(this)
    val soldState: State = SoldState(this)
    val winnerState: State = WinnerState(this)
    var state: State = if (numberGumballs > 0) noQuarterState else soldOutState

    fun insertQuarter() {
        state.insertQuarter()
    }

    fun ejectQuarter() {
        state.ejectQuarter()
    }

    fun turnCrank() {
        state.turnCrank()
        state.dispense()
    }

    fun releaseBall() {
        println("A gumball comes rolling out the slot...")
        if (numberGumballs != 0) {
            numberGumballs -= 1
        }
    }
}
